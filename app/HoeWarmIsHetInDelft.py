import requests

SOURCE_URL = 'http://www.weerindelft.nl/clientraw.txt'

def print_weather(source_url: str):
    """Retrieve weather from source_url and print to stdout.

    Args:
        source_url (str): url providing live weather data in Delft
    """
    response = requests.get(url=source_url)
    response.raise_for_status()
    weather = response.text.split(' ')[4]
    print(weather + ' degrees Celsius')
    
if __name__ == "__main__":
    print_weather(SOURCE_URL)
